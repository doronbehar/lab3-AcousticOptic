this_folder = fileparts(mfilename('fullpath'));

%%% {{{ NEAR FIELD - Generate mean delta_x from csv data
nf_files = {dir([this_folder '/data/2_*.csv']).name};
n = 1;
nf_delta_x = [];
nf_frequencies = [];
nf_errors = [];

% File index (according to glob pattern order) vs MinPeakHeight
% Arab way to do something hard to explain
minimum_peak_height_per_file = struct( ...
);
minimum_peak_distance_per_file = struct( ...
	'file12', 150, ...
	'file11', 200, ...
	'file10', 200, ...
	'file9', 250, ...
	'file8', 120, ...
	'file7', 160, ...
	'file6', 200, ...
	'file5', 300, ...
	'file4', 480, ...
	'file3', 100, ...
	'file2', 120, ...
	'file1', 150 ...
);

for file=nf_files
	% n = 5;
	% file = nf_files(n);
	[p, fname, ext] = fileparts(cell2mat(file));
	fname_split = split(fname, '_');
	frequency = str2double(cell2mat(fname_split(end)));
	fpath = [this_folder '/data/' fname '.csv'];
	data = readtable(fpath);
	% Multiplying by 5.2 to get the data in micrometers and not pixels
	x_data = data{:,:}(:, 1).*5.2;
	y_data = data{:,:}(:, 2);
	fieldname = sprintf('file%d', n);
	if isfield(minimum_peak_distance_per_file, fieldname)
		minPeakDistance = getfield(minimum_peak_distance_per_file, fieldname);
	else
		minPeakDistance = 0;
	end
	if isfield(minimum_peak_height_per_file, fieldname)
		minPeakHeight = getfield(minimum_peak_height_per_file, fieldname);
	else
		minPeakHeight = -Inf;
	end
	[pks, locs, widths] = findpeaks(y_data, x_data, ...
		'MinPeakHeight', minPeakHeight, ...
		'MinPeakDistance', minPeakDistance ...
	);
	nf_error_per_frequency(n) = mean(widths.*0.5).*(1/sqrt(length(diff(locs))));
	nf_delta_x(n) = mean(diff(locs));
	nf_frequencies(n) = frequency;
	n = n + 1;
	current_fig = figure;
	hold on;
	plot(x_data, y_data)
	plot(locs, pks, 'o')
	print(sprintf("%s/data/%s - detected peaks.png", this_folder, fname), "-dpng")
	close(current_fig)
end

%%% }}}

%%% {{{ NEAR FIELD - Perform a fit and plot
% Generated code
nf_periods = nf_frequencies.^(-1);
[xData, yData] = prepareCurveData( nf_periods, nf_delta_x );
ft = fittype( 'poly1' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Lower = [-Inf 0];
opts.Upper = [Inf 0];
[nf_fitresult, nf_gof] = fit( xData, yData, ft, opts );
speed_of_light_in_matter_nf = nf_fitresult.p1*2
% https://www.mathworks.com/matlabcentral/answers/38732-getting-first-element-of-a-function-output#comment_938672
% https://www.mathworks.com/matlabcentral/answers/153547-how-can-i-compute-the-standard-error-for-coefficients-returned-from-curve-fitting-functions-in-the-c
p1_err = abs(nf_fitresult.p1 - table(confint(nf_fitresult)).Var1(1,1));
speed_of_light_in_matter_nf_err = 2*p1_err

nf_fig = figure;
hold on;
errorbar(nf_periods, nf_delta_x, nf_error_per_frequency, 'o')
nf_periods_fit = linspace(0, 1.2, 1000);
plot(nf_periods_fit, ...
	polyval([nf_fitresult.p1 nf_fitresult.p2], ...
	nf_periods_fit) ...
)
legend( ...
	'Measurements', ...
	['Fit: $\Delta X = \frac{v}{2} \cdot f^{-1}$, $R^2 = ' sprintf('%0.3f', nf_gof.rsquare) '$'], ...
	'Location', 'NorthWest', ...
	'FontSize', 13, ...
	'Interpreter', 'Latex' ...
)
xlabel('$T = f^{-1}$ $[\mu sec]$', 'Interpreter', 'Latex')
ylabel('$\Delta X$ $[\mu meter]$', 'Interpreter', 'Latex')
print("near field.png", "-dpng")
close(nf_fig)

%%% }}}

%%% {{{ FAR FIELD - Generate mean delta_x from csv data

ff_files = {dir([this_folder '/data/1_*.csv']).name};
n = 1;
ff_delta_x = [];
ff_frequencies = [];
ff_errors_per_frequency = [];

% File index (according to glob pattern order) vs MinPeakHeight
% Arab way to do something hard to explain
minimum_peak_height_per_file = struct( ...
);
minimum_peak_distance_per_file = struct( ...
	'file9', 500, ...
	'file8', 300, ...
	'file7', 300, ...
	'file5', 350, ...
	'file4', 250, ...
	'file3', 600, ...
	'file2', 600 ...
);
for file=ff_files
	% n = 9;
	% file = ff_files(n);
	[p, fname, ext] = fileparts(cell2mat(file));
	fname_split = split(fname, '_');
	frequency = str2double(cell2mat(fname_split(end)));
	fpath = [this_folder '/data/' fname '.csv'];
	data = readtable(fpath);
	% Multiplying by 5.2 to get the data in micrometers and not pixels
	x_data = (data{:,:}(:, 1)).*5.2;
	y_data = data{:,:}(:, 2);
	y_data_smooth = smoothdata(y_data);
	fieldname = sprintf('file%d', n);
	if isfield(minimum_peak_distance_per_file, fieldname)
		minPeakDistance = getfield(minimum_peak_distance_per_file, fieldname);
	else
		minPeakDistance = 0;
	end
	if isfield(minimum_peak_height_per_file, fieldname)
		minPeakHeight = getfield(minimum_peak_height_per_file, fieldname);
	else
		minPeakHeight = -Inf;
	end
	[pks, locs, widths] = findpeaks(y_data_smooth, x_data, 'Annotate', 'extents', ...
		'MinPeakHeight', minPeakHeight, ...
		'MinPeakDistance', minPeakDistance ...
	);
	ff_error_per_frequency(n) = mean(widths.*0.5).*(1/sqrt(length(diff(locs))));
	current_fig = figure;
	hold on;
	% plot(x_data, y_data)
	plot(x_data, y_data_smooth)
	plot(locs, pks, 'o')
	ylabel('$I$ [relative]', 'Interpreter', 'Latex')
	xlabel('$X$ $[\mu m]$', 'Interpreter', 'Latex')
	print(sprintf("%s/data/%s - detected peaks.png", this_folder, fname), "-dpng")
	close(current_fig)

	ff_delta_x(n) = mean(diff(locs));
	ff_frequencies(n) = frequency;
	n = n + 1;
end

%%% }}}

%%% {{{ FAR FIELD - Perform a fit and plot
% Generated code
[xData, yData] = prepareCurveData( ff_frequencies, ff_delta_x );
ft = fittype( 'poly1' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Lower = [-Inf 0];
opts.Upper = [Inf 0];
[ff_fitresult, ff_gof] = fit( xData, yData, ft, opts );
speed_of_light_in_matter_ff = (30*6328)/ff_fitresult.p1
p1_err = abs(ff_fitresult.p1 - table(confint(ff_fitresult)).Var1(1,1));
speed_of_light_in_matter_ff_err = p1_err * speed_of_light_in_matter_ff/ff_fitresult.p1

ff_fig = figure;
hold on;
errorbar(ff_frequencies, ff_delta_x, ff_error_per_frequency, 'o');
ff_frequencies_fit = linspace(0.9, 5, 1000);
plot(ff_frequencies_fit, ...
	polyval([ff_fitresult.p1 ff_fitresult.p2], ...
	ff_frequencies_fit) ...
)
legend( ...
	'Measurements', ...
	['Fit: $\Delta x = \frac{f_3 \lambda}{v} f$, $R^2 = ' sprintf('%0.3f', ff_gof.rsquare) '$'], ...
	'Location', 'NorthWest', ...
	'FontSize', 13, ...
	'Interpreter', 'Latex' ...
)
xlabel('$f$ [MHz]', 'Interpreter', 'Latex')
ylabel('$\Delta X$ $[\mu m]$', 'Interpreter', 'Latex')
print("far field.png", "-dpng")
close(ff_fig)

%%% }}}

% vim: foldmethod=marker
